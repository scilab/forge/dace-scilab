<?xml version="1.0" encoding="UTF-8"?>
<refentry xmlns="http://docbook.org/ns/docbook" 
	  xmlns:xlink="http://www.w3.org/1999/xlink" 
	  xmlns:svg="http://www.w3.org/2000/svg" 
	  xmlns:mml="http://www.w3.org/1998/Math/MathML" 
	  xmlns:db="http://docbook.org/ns/docbook" 
	  version="5.0-subset Scilab" 
	  xml:lang="en" 
	  xml:id="correxpg">
  <info>
    <pubdate>5-Dec-2007</pubdate>
  </info>
  <refnamediv>
    <refname>correxpg</refname>
    <refpurpose> General exponential correlation function </refpurpose>
  </refnamediv>
  
  <refsynopsisdiv>
    <title>Calling Sequence</title>
    <synopsis>[r,dr] = correxpg(theta,d)</synopsis>
  </refsynopsisdiv>
  
  <refsection>
    <title>Parameters</title>
    <variablelist>
      <varlistentry>
        <term>theta</term>
        <listitem>
          <para>parameters in the correlation function</para>
        </listitem>
      </varlistentry>
      <varlistentry>
        <term>d</term>
        <listitem>
          <para>m*n matrix with differences between given data points</para>
        </listitem>
      </varlistentry>
      <varlistentry>
        <term>r</term>
        <listitem>
          <para>correlation</para>
        </listitem>
      </varlistentry>
      <varlistentry>
        <term>dr</term>
        <listitem>
          <para>m*n matrix with the Jacobian of r at x. It is assumed that x is given implicitely by d(i,:) = x - S(i,:), 
	    where S(i,:) is the i'th design site</para>
        </listitem>
      </varlistentry>
    </variablelist>
  </refsection>
  
  <refsection>
    <title>Description</title>
    <para>General exponential correlation function</para>
  </refsection>
  
  <refsection>
   <title>Examples</title>
   <programlisting role="example"><![CDATA[
theta = [1,2]
m = 20; // nb of points
format(5)

// 2D
n = 2; // dim
d = 2*rand(m,n)-1;
[r,dr] = correxpg(theta, d)
scf();
for i=1:m
    plot([d(i,1)-dr(i,1),d(i,1)],[d(i,2)-dr(i,2),d(i,2)],'--')
    plot([0,d(i,1)],[0,d(i,2)],'colo','black')
end
plot(d(:,1)-dr(:,1),d(:,2)-dr(:,2),'d','MarkSize',8)
plot(d(:,1),d(:,2),'.','markeredg','red','MarkSize',10)
xstring(d(:,1),d(:,2),string(100*r)+'%')
legends(['Points' 'Distance to the center' 'dr (jacobian)'], ..
  [-9 color("black") color("blue") ; color("red") 1 2], "ur");
xtitle("2D")

// 3D
n = 3; // dim
d = 2*rand(m,n)-1;
[r,dr] = correxpg(theta, d)

scf();
for i=1:m
    param3d([0,d(i,1)],[0,d(i,2)],[0,d(i,3)])
end

for i=1:m
    param3d([d(i,1),d(i,1)-dr(i,1)],[d(i,2),d(i,2)-dr(i,2)],[d(i,3),d(i,3)-dr(i,3)])
    k = gce();
    k.foreground = color('blue')
    k.line_style = 2;
    xstring(d(i,1),d(i,2),string(100*r(i))+'%')
    l = gce();
    l.data(3) = d(i,3);
end

param3d(d(:,1),d(:,2),d(:,3))
h = gce();
h.line_mode="off";
h.mark_style=0;
h.mark_size=1;
h.mark_foreground=color('red');

param3d(d(:,1)-dr(:,1),d(:,2)-dr(:,2),d(:,3)-dr(:,3))
h = gce();
h.line_mode="off";
h.mark_style=5;
h.mark_size=0;
h.mark_foreground=color('blue');
legends(['Points' 'Distance to the center' 'dr (jacobian)'], ..
  [-9 color("black") color("blue") ; color("red") 1 2], "ur");
xtitle("3D")


   ]]></programlisting>
</refsection>
  
  <refsection>
    <title>See Also</title>
    <simplelist type="inline">
      <member> 
        <link linkend="correxp">correxp</link> 
      </member>
      <member> 
        <link linkend="corrcubic">corrcubic</link> 
      </member>
      <member> 
        <link linkend="corrgauss">corrgauss</link> 
      </member>
      <member> 
        <link linkend="corrlin">corrlin</link> 
      </member>
      <member> 
        <link linkend="corrspherical">corrspherical</link> 
      </member>
      <member> 
        <link linkend="corrspline">corrspline</link> 
      </member>
      <member> 
        <link linkend="dacefit">dacefit</link> 
      </member>
      <member> 
        <link linkend="predictor">predictor</link> 
      </member>
    </simplelist>
  </refsection>
  
  <refsection>
    <title>Authors</title>
    <variablelist>
      <varlistentry>
	<term>collette</term>
	<listitem>
	  <para> ycollet@freesurf.fr</para>
	</listitem>
      </varlistentry>
    </variablelist>
  </refsection>
</refentry>
